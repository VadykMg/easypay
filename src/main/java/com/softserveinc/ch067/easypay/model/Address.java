package com.softserveinc.ch067.easypay.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(length = 50)
    private String city;
    @Column(length = 100)
    private String street;
    @Column(length = 5)
    private String house;
    @OneToMany
    @CollectionTable(name = "user_services", joinColumns = @JoinColumn(name = "address_id"))
    private Set<Service> services;
    @Column(name = "active")
    private boolean isActive;
}
