package com.softserveinc.ch067.easypay.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private LocalDate lastLoginDate;
    @Enumerated(EnumType.STRING)
    private Role role;
    private double accountBalance;
    @OneToMany
    @CollectionTable(name = "user_addresses", joinColumns = @JoinColumn(name = "user_id"))
    private Set<Address> addresses;
    @Column(name = "active")
    private boolean isActive;
}
