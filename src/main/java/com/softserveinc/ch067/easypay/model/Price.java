package com.softserveinc.ch067.easypay.model;

import javax.persistence.*;

@Entity
@Table(name = "prices")
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private double price;
    @Column(name = "active")
    private boolean isActive;
}
