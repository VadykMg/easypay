package com.softserveinc.ch067.easypay.model;

import javax.persistence.*;

@Entity
@Table(name = "counters")
public class Counter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int count;
    @Column(name = "active")
    private boolean isActive;
}
