package com.softserveinc.ch067.easypay.model;

import javax.persistence.Entity;

public enum Role {
    ADMIN, MANAGER, INSPECTOR, USER
}
