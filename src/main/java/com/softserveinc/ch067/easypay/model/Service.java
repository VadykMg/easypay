package com.softserveinc.ch067.easypay.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "services")
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToMany
    @CollectionTable(name = "service_prices", joinColumns = @JoinColumn(name = "service_id"))
    private Set<Price> price;
    @Column(name = "active")
    private boolean isActive;
    @OneToOne
    @JoinColumn(name = "counter_id")
    private Counter counter;
}
